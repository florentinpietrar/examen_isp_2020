/**
 * @author Florentin Mircea Pietrar
 */
public class s2 {
    public static void main(String[] args) {
        //declaring and naming threads
        Thread1 t1=new Thread1();
        t1.setName("MyThread1");
        Thread2 t2=new Thread2();
        t2.setName("MyThread2");
        Thread3 t3=new Thread3();
        t3.setName("MyThread3");

        //starting the threads
        t1.start();
        t2.start();
        t3.start();

    }
}
class Thread1 extends Thread {
    static int i=1;
    public void run() {
        try {
            while (i<=10) {
                //getting thread name
                Thread t = Thread.currentThread();
                String name = t.getName();
                //displaying the required message
                System.out.println(name+"-"+i++);
                //1 sec delay
                Thread.sleep(1000);
            }
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
class Thread2 extends Thread {
    static int i=1;
    public void run() {
        try {
            while (i<=10) {
                //getting thread name
                Thread t = Thread.currentThread();
                String name = t.getName();
                //displaying the required message
                System.out.println(name+"-"+i++);
                //1 sec delay
                Thread.sleep(1000);
            }
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}
class Thread3 extends Thread {
    static int i=1;
    public void run() {
        try {
            while (i<=10) {
                //getting thread name
                Thread t = Thread.currentThread();
                String name = t.getName();
                //displaying the required message
                System.out.println(name+"-"+i++);
                //1 sec delay
                Thread.sleep(1000);
            }
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}